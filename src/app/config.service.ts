import { Injectable } from '@angular/core';

import { Params } from '../../AWS3.config';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  constructor() { }

  getParams() {
    return Params;
  }
}
