export interface BucketObject {
  'Key': string;
  'LastModified': string;
  'ETag': string;
  'Size': number;
  'StorageClass': string;
}

export interface BucketData extends Array<BucketObject> {}
