import { Injectable } from '@angular/core';
import * as S3 from 'aws-sdk/clients/s3';

import { ConfigService } from './config.service';
import { Params } from '../../AWS3.config';

@Injectable({
  providedIn: 'root'
})
export class S3AccessService {

  // Loading secret Key from config file for now.
  // TODO: use aws cognito ?
  private _cachedData = {};
  private _s3;

  constructor(private _conf: ConfigService) { }

  listFolderObjects = (bucket: string, folder: string) => {
    const promise = new Promise( (resolve, reject) => {
      if (this._cachedData[bucket]) {
        resolve( this._cachedData[bucket].filter(this.hasFolderName(folder)) );
      } else {
        this._s3 = new S3(Params);
        this._s3.listObjectsV2({Bucket: bucket}, (err, data) => {
          if (err) {
            reject(err);
          } else {
            this._cachedData[bucket] = data.Contents;
            const contentList = data.Contents.filter(this.hasFolderName(folder));
            if (folder && ! contentList.length ) {
              reject(`Folder "${folder}" not found in bucket "${bucket}"`);
            } else {
              resolve(contentList);
            }
          }
        });
      }
    });
    return promise;
  }

  // Curried function
  hasFolderName = (folderName: string) => (objectv2) => {
    if (folderName) {
      return objectv2.Key.startsWith(folderName + '/');
    } else {
      return true;
    }
  }

  getBackupObject = (bucket, backupObject, handler) => {
    const params = {
      Bucket: bucket,
      Key: backupObject.Key
    };
    this._s3.getSignedUrl('getObject', params, (err, url) => {
      if (err) {
        handler(err, undefined);
      } else {
        handler(undefined, url);
        window.open(url);
      }
    });
  }
}
