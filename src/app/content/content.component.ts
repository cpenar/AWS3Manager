import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { FolderContentComponent } from '../backup-list/backup-list.component';
import { S3AccessService } from '../s3-access.service';
import { BucketData, BucketObject } from '../bucketData.interface';

@Component({
  selector: 'app-content',
  template: `
    <ul>
        <li class="folderLink" *ngFor="let folder of folderList">
          <a routerLink="/{{bucket}}/{{folder}}">{{folder}}</a>
        </li>
        <li class="bucket">Folders for bucket <h2>{{bucket}}</h2></li>
    </ul>
    <div *ngIf="errorMessage.length">
      Error : {{errorMessage}}. Are you sure the bucket '{{bucket}}' exists ?
    </div>
    <app-backup-list></app-backup-list>
  `,
  styleUrls: [ 'content.component.css' ]
})
export class ContentComponent implements OnInit {

  public folderList = [];
  public backupsList = [];
  public bucket = '';
  public folder = '';
  public errorMessage = '';
  public subscribe;

  constructor(private route: ActivatedRoute, private _s3: S3AccessService) { }

  ngOnInit() {
    this.bucket = this.route.snapshot.params['bucket'];
    this.folder = this.route.snapshot.params['folder'];

    // Retrieve all bucket folders for nav bar
    this._s3.listFolderObjects(this.bucket, '').then( (data: BucketData) => {
      // Select folder name before first '/'
      const tempList = data.map( (obj: BucketObject) => {
        const slashIndex = obj.Key.indexOf('/');
        return obj.Key.substr(0, slashIndex);
      });
      // filter unique folder name
      this.folderList = tempList.filter(
        (folderName, index) => tempList.indexOf(folderName) === index
      );
    }, err => { console.error(err); this.errorMessage = err.message; });
  }
}
