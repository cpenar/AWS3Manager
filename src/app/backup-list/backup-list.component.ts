import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { BucketData, BucketObject } from '../bucketData.interface';
import { S3AccessService } from '../s3-access.service';


@Component({
  selector: 'app-backup-list',
  template: `
    <h2 *ngIf="folder && folder.length">{{folder}}</h2>
    <h3 *ngIf="!(folder && folder.length)">Full content of bucket {{bucket}}</h3>
    <div *ngIf="errorMessage">{{errorMessage}}</div>
    <div *ngIf="backupsList===[]">Retrieving Data, please wait</div>
    <table>
      <thead>
        <tr>
          <th class="path">Path</th>
          <th>Size</th>
          <th>Date</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr class="list-content" *ngFor="let backup of backupList">
          <th class="path">{{cleanPath(backup.Key)}}</th>
          <th>{{backup.Size}}</th>
          <th>{{backup.LastModified.toLocaleString()}}</th>
          <th>
            <button (click)="onDownload(bucket, backup)">Download</button>
          </th>
        </tr>
      </tbody>
    </table>
  `,
  styleUrls: [ 'backup-list.component.css' ]
})
export class FolderContentComponent implements OnInit {

  public errorMessage = '';
  public backupList = [];
  public bucket = '';
  public folder = '';

  constructor (private _s3: S3AccessService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe( params => {
      this.bucket = params['bucket'];
      this.folder = params['folder'];
      this.getBackupList(this.bucket, this.folder);
    });
  }

  getBackupList = (bucket, folder) => {
    this._s3.listFolderObjects(bucket, folder).then(
      (data: BucketData) => {
        this.backupList = data
          .filter(backup => backup.Size !== 0)
          .sort(this.sortByLastModified);
      }, err => console.error(err) );
  }

  cleanPath = (path) => {
    if (this.folder) {
      const slashIndex = path.indexOf('/');
      return path.substr(slashIndex + 1);
    } else {
      return path;
    }
  }

  sortByLastModified = (objectV2_A, objectV2_B) => {
    return objectV2_B.LastModified.getTime() - objectV2_A.LastModified.getTime();
  }

  onDownload = (bucket, backupObject) => {
    this._s3.getBackupObject(bucket, backupObject, (err, data) => {
      if (err) {
        console.error(err);
        this.errorMessage = err;
      } else {
        console.log(data);
      }
    });
  }
}
