import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContentComponent } from './content/content.component';
import { ConfigService } from './config.service';

const confServ = new ConfigService;
const defaultRoute = confServ.getParams().defaultBucket;

const routes: Routes = [
  { path: ':bucket/:folder', component: ContentComponent },
  { path: ':bucket', pathMatch: 'full', component: ContentComponent },
  { path: '', redirectTo: defaultRoute, pathMatch: 'full' }
];

@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes) ]
})
export class AppRoutingModule { }
