import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <div class="main-content">
      <h1>{{title}}</h1>
      <router-outlet></router-outlet>
    </div>
  `,
  styles: [
    '.main-content { width: 80%; margin: auto; margin-top:0px;',
    'h1 { background-color: #1A4044; color: #CCC; padding: 1em;margin: 0px;  }'
  ]
})
export class AppComponent {
  title = 'AWS S3 Simple Manager';
}
