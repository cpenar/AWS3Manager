import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { S3AccessService } from './s3-access.service';
import { ConfigService } from './config.service';
import { AppComponent } from './app.component';
import { FolderContentComponent } from './backup-list/backup-list.component';
import { AppRoutingModule } from './/app-routing.module';
import { ContentComponent } from './content/content.component';

@NgModule({
  declarations: [
    AppComponent,
    FolderContentComponent,
    ContentComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule
  ],
  providers: [
    S3AccessService,
    ConfigService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
