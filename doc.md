### RESSOURCES

https://aws.amazon.com/developers/getting-started/browser/

[AWS SDK for JavaScript forum](https://forums.aws.amazon.com/forum.jspa?forumID=148)

[AWS SDK for JavaScript in the Browser](https://aws.amazon.com/sdk-for-browser/)

[AWS SDK Gitter messaging](https://gitter.im/aws/aws-sdk-js)

http://www.ng-newsletter.com/posts/aws-js-sdk.html

### SPECIFIC

From [stackoverflow/Using aws-sdk with angular2](https://stackoverflow.com/questions/37041049/using-aws-sdk-with-angular2)

https://www.npmjs.com/package/aws-sdk

[Workaround for the  ReferenceError: global is not defined](https://github.com/aws/aws-amplify/issues/678)

[AWS Amplify provides Angular Components](https://aws.github.io/aws-amplify/media/angular_guide?utm_source=aws-js-sdk&utm_campaign=angular)

[Using aws-sdk with Angular](https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/#With_Angular)


