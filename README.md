# AWS3Manager

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.3.

## Requirements

* Run `npm install` to install dependencies.

* If you dont have an **IAM** user allready you need to create one [here](https://console.aws.amazon.com/iam/home#/users$new?step=details). And save your **_accessKeyId_** and **_secretAccessKey_**. Cognito authentication is not implemented yet.

* Copy `AWS3.config.template.ts` to `AWS3.config.ts`, and fill in the blanks.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
